import java.util.*;

public class Node {

	private String name;
	private Node firstChild;
	private Node nextSibling;

	Node(String n, Node d, Node r) {
		this.name = n;
		this.firstChild = d;
		this.nextSibling = r;
	}

	public static Node parsePostfix(String s) {
		OtsiViga(s);
		Stack<Node> stack = new Stack<>();
		Node uusNode = new Node(null, null, null);
		StringTokenizer st = new StringTokenizer(s, "(),", true);
		while (st.hasMoreTokens()) {
			String token = st.nextToken().trim();
			if (token.equals("(")) {
				stack.push(uusNode);
				uusNode.firstChild = new Node(null, null, null);
				uusNode = uusNode.firstChild;
			} else if (token.equals(")")) {
				Node node = stack.pop();
				uusNode = node;
			} else if (token.equals(",")) {
				if (stack.empty())
					throw new RuntimeException("Comma exception" + s);
				uusNode.nextSibling = new Node(null, null, null);
				uusNode = uusNode.nextSibling;
			} else {
				uusNode.name = token;
			}
		}
		return uusNode;
	}

	public String leftParentheticRepresentation() {
		return toLeft(this);
	}

	// inspiratsioona saadud:
	// https://bitbucket.org/Sgreef/home5/src/a2641a3943b4fc5e772ea23136a2c01f61a469e0/src/Node.java?at=master&fileviewer=file-view-default

	public static String toLeft(Node juur) {
		StringBuilder ret = new StringBuilder("");
		ret.append(juur.name);
		if (juur.firstChild != null) {
			ret.append("(");
			ret.append(toLeft(juur.firstChild));
			ret.append(")");
			if (juur.nextSibling != null) {
				ret.append(",");
				ret.append(toLeft(juur.nextSibling));
			}
		} else if (juur.nextSibling != null) {
			ret.append(",");
			ret.append(toLeft(juur.nextSibling));
		}
		System.out.println(ret.toString());
		return ret.toString();
	}

	public static void main(String[] param) {
		String s = "(B1,C)A";
		Node t = Node.parsePostfix(s);
		String v = t.leftParentheticRepresentation();
		System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
	}

	public static void OtsiViga(String s) {
		if (s.length() == 0)
			throw new RuntimeException("Puu on t�hi " + s);
		if (!s.matches("[\\w(),+--/ *]+"))
			throw new RuntimeException("String sisaldab valesid s�mboleid: " + s);
		if (s.contains(" "))
			throw new RuntimeException("Stringis on t�hikud: " + s);
		if (s.contains(",,"))
			throw new RuntimeException("String sisaldab topelt komasid: " + s);
		if (s.contains("()"))
			throw new RuntimeException("String sisaldab t�hja puud: " + s);
		if (s.contains(",") && !s.contains("(") && !s.contains(")"))
			throw new RuntimeException("String sisaldab topelt juuri: " + s);
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '(' && s.charAt(i + 1) == ',')
				throw new RuntimeException("P�rast sulge ei saa olla koma: " + s);
			if (s.charAt(i) == ')' && (s.charAt(i + 1) == ',' || s.charAt(i + 1) == ')'))
				throw new RuntimeException("Toplet paremsulg: " + s);
		}

	}
}
